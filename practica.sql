-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-06-2017 a las 17:58:43
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `practica`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `agregarCurso` (IN `_nombre` VARCHAR(50))  BEGIN
	DECLARE _cursoExistente INT;
	SET _cursoExistente = (SELECT Count(*) FROM curso WHERE nombreCurso = _nombre);

	IF(_cursoExistente = 0) THEN
		INSERT INTO curso(nombreCurso)
        VALUES (_nombre);
	END IF;
	select * from curso;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `agregarDetalle` (IN `_idGrupo` INT, IN `_idCurso` INT)  BEGIN
	
		INSERT INTO detallegrupo(idGrupo, idCurso)
        VALUES (_idGrupo, _idCurso);
	
	select * from muestradetalle;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `agregarGrupo` (IN `_nombre` VARCHAR(50))  BEGIN
	DECLARE _grupoExistente INT;
	SET _grupoExistente = (SELECT Count(*) FROM grupo WHERE nombreGrupo = _nombre);

	IF(_grupoExistente = 0) THEN
		INSERT INTO grupo(nombreGrupo)
        VALUES (_nombre);
	END IF;
	select * from Grupo;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE `curso` (
  `idCurso` int(11) NOT NULL,
  `nombreCurso` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `curso`
--

INSERT INTO `curso` (`idCurso`, `nombreCurso`) VALUES
(1, 'Matematica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallegrupo`
--

CREATE TABLE `detallegrupo` (
  `idDetalleGrupo` int(11) NOT NULL,
  `idGrupo` int(11) NOT NULL,
  `idCurso` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detallegrupo`
--

INSERT INTO `detallegrupo` (`idDetalleGrupo`, `idGrupo`, `idCurso`) VALUES
(3, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `idGrupo` int(11) NOT NULL,
  `nombreGrupo` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`idGrupo`, `nombreGrupo`) VALUES
(1, 'IN6AM');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `muestradetalle`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `muestradetalle` (
`idDetalleGrupo` int(11)
,`idGrupo` int(11)
,`nombreGrupo` varchar(20)
,`idCurso` int(11)
,`nombreCurso` varchar(20)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `muestradetalle`
--
DROP TABLE IF EXISTS `muestradetalle`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `muestradetalle`  AS  select `d`.`idDetalleGrupo` AS `idDetalleGrupo`,`g`.`idGrupo` AS `idGrupo`,`g`.`nombreGrupo` AS `nombreGrupo`,`c`.`idCurso` AS `idCurso`,`c`.`nombreCurso` AS `nombreCurso` from ((`detallegrupo` `d` join `grupo` `g` on((`d`.`idGrupo` = `g`.`idGrupo`))) join `curso` `c` on((`d`.`idCurso` = `c`.`idCurso`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`idCurso`);

--
-- Indices de la tabla `detallegrupo`
--
ALTER TABLE `detallegrupo`
  ADD PRIMARY KEY (`idDetalleGrupo`),
  ADD KEY `idGrupo` (`idGrupo`),
  ADD KEY `idCurso` (`idCurso`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`idGrupo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `curso`
--
ALTER TABLE `curso`
  MODIFY `idCurso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `detallegrupo`
--
ALTER TABLE `detallegrupo`
  MODIFY `idDetalleGrupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `grupo`
--
ALTER TABLE `grupo`
  MODIFY `idGrupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
