

var modelGrupo = function() {
  var main = this;
  var detalleGrupoUri = "api/detalleGrupo";
  main.detellesGrupo = ko.observableArray([]);

  function ajaxHelper(uri, method, data) {
    return $.ajax({
      url: uri,
      type: method,
      dataType: 'json',
      contentType: 'application/json',
      data: data ? JSON.stringify(data) : null
    }).fail(function(jqXHR, textStatus, errorThrown){
      console.log(errorThrown);
    });
  }

  main.getDetallesGrupo = function() {
    ajaxHelper(detalleGrupoUri, 'GET').done(function(data) {
      main.detallesGrupo(data);
    });
  }
  main.getDetallesGrupo();
}


$(document).ready(function() {
  var ModelGrupo = new modelGrupo();
  ko.applyBindings(ModelGrupo);
})
