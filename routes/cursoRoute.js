var express = require('express');
var curso = require('../model/curso');
var router = express.Router();


router.post('/api/curso', function(req, res) {
  var data = {
    idCurso : null,
    nombreCurso: req.body.nombreCurso
  }
  curso.insert(data, function(err, resultado) {
    if(resultado && resultado.insertId > 0) {
      res.redirect('/api/curso');
    } else {
      res.json({"Mensaje": "No se ingreso la curso"});
    }
  });
});


module.exports = router;
