var express = require('express');
var grupo = require('../model/grupo');
var router = express.Router();



router.post('/api/grupo', function(req, res) {
  var data = {
    idGrupo : null,
    nombreGrupo: req.body.nombreGrupo
  }
  grupo.insert(data, function(err, resultado) {
    if(resultado && resultado.insertId > 0) {
      res.redirect('/api/grupo');
    } else {
      res.json({"Mensaje": "No se ingreso la grupo"});
    }
  });
});


module.exports = router;
