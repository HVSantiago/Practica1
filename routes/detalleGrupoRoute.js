var express = require('express');
var detalleGrupo = require('../model/detalleGrupo');
var router = express.Router();

router.get('/api/detalleGrupo/', function(req, res) {
  categoria.selectAll(function(error, resultados){
    if(typeof resultados !== undefined) {
      res.json(resultados);
    } else {
      res.json({"Mensaje": "No hay categorias"});
    }
  });
});

router.post('/api/detalleGrupo', function(req, res) {
  var data = {
    idDetalle : null,
    idGrupo: req.body.idGrupo,
    idCurso: req.body.idCurso

  }
  detalleGrupo.insert(data, function(err, resultado) {
    if(resultado && resultado.insertId > 0) {
      res.redirect('/api/detalleGrupo');
    } else {
      res.json({"Mensaje": "No se ingreso la detalleGrupo"});
    }
  });
});


module.exports = router;
