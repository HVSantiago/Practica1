var database = require('./database');
var grupo = {};


grupo.insert = function(data, callback) {
  if(database) {
    database.query("CALL agregarGrupo SET ? ", data,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

module.exports = grupo;
