var database = require('./database');
var detalleGrupo = {};

detalleGrupo.selectAll = function(callback) {
  if(database) {
    database.query("SELECT * FROM muestradetalle",
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

detalleGrupo.insert = function(data, callback) {
  if(database) {
    database.query("CALL agregarDetalle(?,?)", data,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

module.exports = detalleGrupo;
