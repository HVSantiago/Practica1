var database = require('./database');
var curso = {};

curso.insert = function(data, callback) {
  if(database) {
    database.query("CALL agregarCurso SET", data,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

module.exports = curso;
